#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SHMSZ 1024

int main() { //shmat(klucz)
	key_t key;
	int shmid;
	unsigned char *shared;
	//unsigned char tab [50000]; //tab[0] is a state of 5 same bytes in row
	
	if((key = ftok("zapisz.c", 'C')) == -1) {
		perror("ftok returned error");
		return 1;
	}
	unsigned char * tab;
	tab = malloc(SHMSZ * sizeof(unsigned char));
	if((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) {
		perror("shmget returned error");
		return 1;
	}
	if((shared = shmat(shmid, NULL, 0)) == (char *) -1) {
		perror("shmat returned error");
		return 1;
	}
	tab = shared; //shm to adres shared
	tab[0] = 1;
	srand(time(NULL));
	int i = 1; 
	for(i = 1; tab[0] != 0; i++) { 
		if(i == SHMSZ - 1) i = 1;
		
		  tab[i] = rand() % 256; 
		  if(tab[0] == 0) break;

		  }
	
	return 0;
}
