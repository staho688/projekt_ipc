#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>

#define SHMSZ 1024

int main() {
	key_t key;
	int shmid;
	unsigned char *shared, *data;

	if((key = ftok("zapisz.c", 'C')) == -1) {
		perror("ftok returned error");
		return 1;
	}
	if((shmid = shmget(key, SHMSZ, 0666)) < 0) {
		perror("shmget returned error");
		return 1;
	}
	if((shared = shmat(shmid, (void *)0, 0)) == (char *) -1) {
		perror("shmat returned error");
		return 1;
	}
	unsigned char x = (int)0;
	int i = 1, cnt = 0;
	
	data = shared;
	for(i = 1; data[0] != 0; i++) {
		if(i == SHMSZ - 1) i = 1;
	
		if(data[i] != x){
			x = data[i];
			cnt = 0;
		}
		else cnt++;
		
		if(cnt == 5) {
			printf("Found 5 same bytes/n");
			data[0] = 0; 
			break;
		}
	}
	
	return 0;
}
